/**
 * 
 * @param {Date} time 任意可以被Date函数解析的时间字符
 * @param {String} format 返回的时间格式
 * @param {Boolean} utc true 将格林时间转为本地时间，false将本地时间转为格林时间
 */

export const formatDate = function (
  time = new Date(),
  format = "YYYY-MM-DD hh:mm:ss",
  utc = null
) {
  try {
    time = new Date(time);
  } catch (error) {
    console.error("Wrong time type：", error);
    time = new Date();
  }

  if (utc === false)
    time = new Date(
      time.getTime() - new Date().getTimezoneOffset() * 60 * 1000
    );

  if (utc === true)
    time = new Date(
      time.getTime() + new Date().getTimezoneOffset() * 60 * 1000
    );
  [
    { test: /YYYY/g, text: time.getFullYear() },
    { test: /MM/g, text: time.getMonth() + 1 },
    { test: /DD/g, text: time.getDate() },
    { test: /hh/g, text: time.getHours() },
    { test: /mm/g, text: time.getMinutes() },
    { test: /ss/g, text: time.getSeconds() },
    { test: /ms/g, text: time.getMilliseconds() }
  ].forEach(e => {
    format = format.replace(e["test"], e.text.toString().padStart(2, '0'));
  });
  return format;
};
export default formatDate;
