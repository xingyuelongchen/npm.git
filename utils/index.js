
import Drag from './el-drag-dialog';
import Clipboard from './clipboard';
import waves from './waves/waves';
export const Axios = require('./axios').default;
export const Storage = require('./storage').default;
export const FormatDate = require('./formatDate').default;
export const Validate = require('./validate');
export const sketchpad = require('./sketchpad/sketchpad').default
export * from './storage';
export * from './validate';
export * from './formatDate';
const obj = { Axios, Storage, Validate, ...Storage, ...Validate, FormatDate, sketchpad };
export default {
    ...obj,
    install(VM) {
        // 指令
        VM.directive('drag-dialog', Drag);
        VM.directive('copy', Clipboard);
        VM.directive('waves', waves);
        // 过滤器
        VM.filter('formatDate', (val, type, utc) => {
            if (!val) return '';
            return FormatDate(val, type, utc)
        });
        // var version = Number(VM.version.split('.')[0]);
        // if (version == 3) {
        //     // vue ^3.0  版本扩展方法
        //     VM.config.globalProperties = obj;
        // } else {
        //     // vue 2.0+  版本扩展方法
        //     Object.keys(obj).forEach(e => {
        //         VM.prototype[e.toLocaleLowerCase()] = obj[e]
        //     })
        // }
    }
};
/**
 * 缓存tab选项卡
 */
// function cache() {
//     window.onbeforeunload = () => {
//         try {
//             delete Store.state.nocach;
//             sessionStorage.setItem("Store", JSON.stringify(Store.state));

//         } catch (error) {
//             console.log(error);
//             return false

//         }
//     };
//     window.addEventListener("load", () => {
//         let data = sessionStorage.getItem("Store") || false;
//         if (data) Store.commit("setInit", JSON.parse(data));
//         sessionStorage.removeItem("Store");
//     });
// }

